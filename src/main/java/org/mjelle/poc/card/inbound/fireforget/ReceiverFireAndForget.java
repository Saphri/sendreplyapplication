package org.mjelle.poc.card.inbound.fireforget;

import org.mjelle.poc.card.model.Email;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ReceiverFireAndForget {
	@JmsListener(destination = "${myqueues.fireandforget}")
	public void receiveMessage(Email email) {
		log.info("FireAndForget Received <{}>", email);
	}
}
