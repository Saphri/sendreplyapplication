package org.mjelle.poc.card.inbound.sendreply;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.mjelle.poc.card.model.Email;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class ReceiverSendAndReply {
	private final JmsTemplate jmsTemplate;
    private final MessageConverter converter;

	@JmsListener(destination = "${myqueues.sendandreply.send}")
	public void receiveMessage(Email email, Message message) throws Exception {
        log.info("SendAndReply Received: <{}>", email);
    	Thread.sleep((long)(Math.random() * 25000));
		jmsTemplate.setExplicitQosEnabled(true);
		jmsTemplate.setTimeToLive(30000l);
        jmsTemplate.send(message.getJMSReplyTo(), new MessageCreator() {

            @Override
            public Message createMessage(Session session) throws JMSException {
                Message responseMsg = converter.toMessage(new Email(String.format("Pong: %s", message.getJMSCorrelationID())), session) ;
                responseMsg.setJMSCorrelationID(message.getJMSCorrelationID());
                return responseMsg;
            }
        });
	}
}
