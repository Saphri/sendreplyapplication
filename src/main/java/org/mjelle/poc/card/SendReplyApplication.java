package org.mjelle.poc.card;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SendReplyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SendReplyApplication.class, args);
    }
}