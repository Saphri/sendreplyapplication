package org.mjelle.poc.card.config;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.JmsException;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.connection.ConnectionFactoryUtils;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.SessionCallback;
import org.springframework.jms.support.JmsUtils;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

@Configuration
@EnableJms
public class MQConfig {
	@Bean
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_type");
		return converter;
	}

	@Bean
	public MyJmsTemplate setupTemplate(ConnectionFactory connectionFactory) {
		MyJmsTemplate template = new MyJmsTemplate(connectionFactory);
		template.setSessionTransacted(false);
		template.setMessageConverter(jacksonJmsMessageConverter());
		return template;
	}

	public class MyJmsTemplate extends JmsTemplate {

		public MyJmsTemplate(ConnectionFactory connectionFactory) {
			super(connectionFactory);
		}

		public Message sendAndReceive(final String destinationName, final String responseQueue,
				final MessageCreator messageCreator) throws JmsException {
			return executeLocal(session -> {
				Destination destination = resolveDestinationName(session, destinationName);
				Destination replyTo = resolveDestinationName(session, responseQueue);
				return doSendAndReceive(session, destination, replyTo, messageCreator);
			}, true);
		}

		/**
		 * A variant of {@link #execute(SessionCallback, boolean)} that explicitly
		 * creates a non-transactional {@link Session}. The given
		 * {@link SessionCallback} does not participate in an existing transaction.
		 */
		@Nullable
		private <T> T executeLocal(SessionCallback<T> action, boolean startConnection) throws JmsException {
			Assert.notNull(action, "Callback object must not be null");
			Connection con = null;
			Session session = null;
			try {
				con = createConnection();
				session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
				if (startConnection) {
					con.start();
				}
				if (logger.isDebugEnabled()) {
					logger.debug("Executing callback on JMS Session: " + session);
				}
				return action.doInJms(session);
			} catch (JMSException ex) {
				throw convertJmsAccessException(ex);
			} finally {
				JmsUtils.closeSession(session);
				ConnectionFactoryUtils.releaseConnection(con, getConnectionFactory(), startConnection);
			}
		}

		/**
		 * Send a request message to the given {@link Destination} and block until a
		 * reply has been received on a response queue.
		 * <p>
		 * Return the response message or {@code null} if no message has
		 * 
		 * @throws JMSException if thrown by JMS API methods
		 */
		@Nullable
		protected Message doSendAndReceive(Session session, Destination destination, Destination responseQueue,
				MessageCreator messageCreator) throws JMSException {
			Assert.notNull(messageCreator, "MessageCreator must not be null");
			MessageProducer producer = null;
			MessageConsumer consumer = null;
			try {
				Message requestMessage = messageCreator.createMessage(session);
				producer = session.createProducer(destination);
				consumer = session.createConsumer(responseQueue,
						"JMSCorrelationID='" + requestMessage.getJMSCorrelationID() + "'");
				requestMessage.setJMSReplyTo(responseQueue);
				if (logger.isDebugEnabled()) {
					logger.debug("Sending created message: " + requestMessage);
				}
				doSend(producer, requestMessage);
				return receiveFromConsumer(consumer, getReceiveTimeout());
			} finally {
				JmsUtils.closeMessageConsumer(consumer);
				JmsUtils.closeMessageProducer(producer);
			}
		}
	}
}
