package org.mjelle.poc.card.outbound.sendreply;

import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.mjelle.poc.card.config.MQConfig.MyJmsTemplate;
import org.mjelle.poc.card.model.Email;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
public class SenderSendAndReply {
	
	@Value("${myqueues.sendandreply.send}")
	private String sendQueueName;
	@Value("${myqueues.sendandreply.reply}")
	private String replyQueueName;
	
	private final MyJmsTemplate jmsTemplate;
    private final MessageConverter converter;
    
	@Scheduled(initialDelay = 10000, fixedRate = 5000)
	@Async
	public void send() throws MessageConversionException, JMSException {
        log.info("SendAndReply Sending an email message.");
		jmsTemplate.setReceiveTimeout(60000l);
		jmsTemplate.setExplicitQosEnabled(true);
		jmsTemplate.setTimeToLive(30000l);
        final String msgId = UUID.randomUUID().toString();
        
        Message received = jmsTemplate.sendAndReceive(sendQueueName, replyQueueName, new MessageCreator() {

            @Override
            public Message createMessage(Session session) throws JMSException {
                Message message = converter.toMessage(new Email(String.format("Ping: %s", msgId)), session);
                message.setJMSCorrelationID(msgId);
                return message;
            }
        });
        if(received != null) {
        	Email email = (Email) converter.fromMessage(received);
        	log.info("SendAndReply got answered: <{}>", email);
        	if(email.getBody().contains(msgId)) {
        		log.debug("msgId was valid");
        	} else {
        		throw new IllegalStateException("msgId was not correct!");
        	}
        } else {
        	log.warn("SendAndReply got timeout!");
        }
	}	
}
