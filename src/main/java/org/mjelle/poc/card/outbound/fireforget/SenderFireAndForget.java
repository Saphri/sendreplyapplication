package org.mjelle.poc.card.outbound.fireforget;

import org.mjelle.poc.card.model.Email;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
public class SenderFireAndForget {
	
	@Value("${myqueues.fireandforget}")
	private String queueName;

	private final JmsTemplate jmsTemplate;
	
	@Scheduled(initialDelay = 5000, fixedRate = 5000)
	@Async
	public void send() {
        // Send a message with a POJO - the template reuse the message converter
        log.info("FireAndForget Sending an email message.");
        jmsTemplate.convertAndSend(queueName, new Email("Is anybody out there?"));
	}
}
