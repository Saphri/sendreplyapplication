# Example for using IBM MQ JMS with Spring Boot 2.1

###### Requirements:
* IBM MQ
* Java JDK
* Internet access

### Starting example
To use the example you need to have access to an IBM MQ somewhere. Then you modify the application.yml to point to your IBM MQ so you can connect correctly.

The values shown in the current application.yml is default when starting IBM MQ via the Docker image.

```
ibm.mq:
  queueManager: QM1
  channel: DEV.ADMIN.SVRCONN
  connName: localhost(1414)
  user: admin
  password: passw0rd
```
adapt these as needed.

This example use 3 queue defined in the application.yml:
```
myqueues:
  fireandforget: DEV.QUEUE.1
  sendandreply:
    send: DEV.QUEUE.2
    reply: DEV.QUEUE.3
```
also adapt these as needed.

### Fire and forget
Fire and forget sender is triggered every 5 seconds. The fire and forget listener simply logs the message.

### Send and reply
Send and reply sender is triggered every 5 seconds. The send and reply listener waits randomly up to 25 seconds before sending back a reply. The sender then receives the reply and logs it.
